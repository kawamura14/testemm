var url = "https://teste-mm.firebaseio.com/phonebook.json"
var userData = [];

// Update table according to data
var updateTable = async function () {
	var dataTable = document.getElementById('table1'),
		tableHead = document.getElementById('table-head'),
		tbody = document.createElement('tbody');

	while (dataTable.firstChild) {
		dataTable.removeChild(dataTable.firstChild);
	}
	dataTable.appendChild(tableHead);

	for (var [i, items] of userData.entries()) {
		var tr = document.createElement('tr'),
			name = document.createElement('td'),
			phone = document.createElement('td'),
			email = document.createElement('td'),
			edit = document.createElement('td'),
			del = document.createElement('td'),
			btnDelete = document.createElement('input'),
			btnEdit = document.createElement('input');

		btnDelete.setAttribute('type', 'button');
		btnDelete.setAttribute('value', 'Deletar');
		btnDelete.setAttribute('class', 'btnDelete');
		btnDelete.setAttribute('id', i);

		btnEdit.setAttribute('type', 'button');
		btnEdit.setAttribute('value', 'Editar');
		btnEdit.setAttribute('id', i);

		tr.appendChild(name);
		tr.appendChild(phone);
		tr.appendChild(email);
		tr.appendChild(edit);
		tr.appendChild(del);

		name.innerHTML = items.name;
		phone.innerHTML = items.phone;
		email.innerHTML = items.email;
		edit.appendChild(btnEdit);
		del.appendChild(btnDelete);

		btnDelete.onclick = (function () {
			return async function () {
				if (confirm("Deseja realmente deletar esse item?")) {
					var deleteId = this.getAttribute('id');
					userData.splice(deleteId, 1);
					await updateAPI(userData);
					updateTable();
				}
			};
		})();

		btnEdit.addEventListener('click', function () {
			document.getElementById('id01').style.display = 'block';
			var editId = this.getAttribute('id');
			updateForm(editId);
		}, false);

		tbody.appendChild(tr);
	}
	dataTable.appendChild(tbody);
}

// Set form for data edit
var updateForm = function (id) {
	var nameField = document.getElementById('name'),
		phoneField = document.getElementById('phone'),
		emailField = document.getElementById('email'),
		saveButton = document.getElementById('btnSave');

	nameField.value = userData[id].name;
	emailField.value = userData[id].email;
	phoneField.value = userData[id].phone;
	saveButton.value = 'Editar';
	saveButton.setAttribute('data-update', id);
}

//Funtion update API
var updateAPI = async function (data) {
	await fetch(url, {
		method: 'PUT',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json'
		}
	});
}

// Save new data
var saveData = async function () {
	try {
		var newName = document.getElementById('name').value,
			newPhone = document.getElementById('phone').value,
			newEmail = document.getElementById('email').value,
			datatoAdd = {
				id: Date.now(),
				name: newName,
				phone: newPhone,
				email: newEmail,
			};
		userData.push(datatoAdd);
		await updateAPI(userData);
		updateTable();
	} catch (error) {
		alert('Ocorreu um erro')
	}
}

// Update data
var updateData = async function (id) {
	var upName = document.getElementById('name').value,
		upPhone = document.getElementById('phone').value;
	upEmail = document.getElementById('email').value;

	userData[id].name = upName;
	userData[id].phone = upPhone;
	userData[id].email = upEmail;
	await updateAPI(userData);
	updateTable();
}

// Reset the form
var refreshForm = function () {
	var nameField = document.getElementById('name'),
		phoneField = document.getElementById('phone'),
		emailField = document.getElementById('email'),
		saveButton = document.getElementById('btnSave');

	nameField.value = '';
	phoneField.value = '';
	emailField.value = '';
	saveButton.value = 'Save';
	saveButton.removeAttribute('data-update');
}

var openModal = function () {
	document.getElementById('id01').style.display = 'block';
	// refreshForm();
}

var closeModal = function () {
	document.getElementById('id01').style.display = 'none';
	refreshForm();
}

// Main function
var init = async function () {
	try {
		const response = await fetch(url);
		userData = await response.json();
		if (userData === null) {
			userData = [];
		}
	} catch (error) {
		alert('Ocorreu um erro')
	}
	updateTable();
	var btnSave = document.getElementById('btnSave');
	btnSave.onclick = function () {
		if (btnSave.getAttribute('data-update')) {
			updateData(btnSave.getAttribute('data-update'));
		} else {
			saveData();
		}
		closeModal();
	};
};

// Mask phone number
const formatToPhone = (event) => {
	const target = event.target;
	const input = event.target.value.replace(/\D/g, '').substring(0, 11);
	const ddd = input.substring(0, 2);
	const middle = input.substring(2, 7);
	const last = input.substring(7, 11);

	if (input.length > 7) { target.value = `(${ddd}) ${middle} - ${last}`; }
	else if (input.length > 2) { target.value = `(${ddd}) ${middle}`; }
	else if (input.length > 0) { target.value = `(${ddd}`; }
};
const inputElement = document.getElementById('phone');
inputElement.addEventListener('keyup', formatToPhone);

init(); //Intialize the table